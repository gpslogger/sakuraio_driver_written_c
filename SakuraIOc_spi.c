#include "SakuraIOc_spi.h"
#include "spi_interface.h"  //コンパイルできるようにするためのダミーファイル

inline void delay(uint32_t ms){
    CyDelay(ms);
}

void SakuraIOc_hw_init(){
    //元関数のコンストラクタ
    //SPIの初期化をする
}

void SakuraIOc_hw_begin(){
    //SPIを転送可能状態にする関数
    //CSをアサート
}

void SakuraIOc_hw_end(){
    //SPIを転送終了状態にする関数
    //CSをネゲート
    //元関数では最後に20us待ってる
}

void SakuraIOc_hw_sendByte(uint8_t data){
    //SPIで1byte送る関数
    //元関数では送る前に20us待ってる
}

void SakuraIOc_hw_RxBufClr(){
    //SPIの受信バッファをクリアする関数
}

uint8_t SakuraIOc_hw_startReceive(uint8_t length){
    //I2C用の関数なのでSPIはダミー動作させる
    return length;
}

uint8_t SakuraIOc_hw_receiveByte(){
    //SPIから1byte読む関数
    //元の関数では読む前に20us待ってる
    //送信ダミーデータは0x00にしてある
    return 0;
}

uint8_t SakuraIOc_hw_receiveByte2(bool stop){
    //元の関数ではstopが使われてない．
    return 0;
}
